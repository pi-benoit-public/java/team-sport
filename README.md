# Projet Java Team Sport

WIP - Travail en cours

## Description

Application qui gère des équipes de sports différentes (basket, baseball, football, tennis, handball, cricket...) avec des joueurs différents.

On pourra créer plusieurs équipes par sport et les faire s'affronter. L'équipe qui comptabilise le plus de points sera classée la première.
