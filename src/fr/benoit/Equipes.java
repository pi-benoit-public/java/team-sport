package fr.benoit;

import java.util.List;

public class Equipes {

	private String nom;
	private Sports sport;
	private List<Joueurs> joueur;

	public Equipes(String nom, List<Joueurs> joueur) {
		super();
		this.nom = nom;
		this.joueur = joueur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Joueurs> getJoueur() {
		return joueur;
	}

	public void setJoueur(List<Joueurs> joueur) {
		this.joueur = joueur;
	}

}
