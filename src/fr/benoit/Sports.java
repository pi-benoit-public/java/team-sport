package fr.benoit;

import java.util.List;

public enum Sports {

	// WIP
	BASKET("Basket", null, null);

	private String nom;
	private List<Equipes> equipes;
	private List<Joueurs> joueurs;

	private Sports(String nom, List<Equipes> equipes, List<Joueurs> joueurs) {
		this.nom = nom;
		this.equipes = equipes;
		this.joueurs = joueurs;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Equipes> getEquipes() {
		return equipes;
	}

	public void setEquipes(List<Equipes> equipes) {
		this.equipes = equipes;
	}

	public List<Joueurs> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(List<Joueurs> joueurs) {
		this.joueurs = joueurs;
	}

}
